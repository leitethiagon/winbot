﻿/***************************************************************************
 * 
 *   copyright: Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace Winbot.Tests
{
    [TestFixture]
    public class RobotTests
    {
        private Robot myrobot;

        [SetUp]
        public void setup()
        {
            myrobot = new Robot("My robot", @"\Scripts");
        }

        [Test]
        public void can_create_robot()
        {
            myrobot = new Robot("New robot", @"\Scripts");

            Assert.AreEqual("New robot", myrobot.Name);
        }

        [Test]
        public void robot_add_hear_handler_and_do_something()
        {
            myrobot.Hear("hello", (robot, msg) => robot.Send("Hello world"));

            Assert.AreEqual("hello", myrobot.Handlers.FirstOrDefault().Pattern);
        }

        [Test]
        public void response_handler_must_have_robot_name_on_pattern()
        {
            myrobot.Respond("hello|hi", (robot,msg) => robot.Send("Hello world"));

            Assert.AreEqual("My robot hello|hi", myrobot.Handlers.FirstOrDefault().Pattern);
        }
    }
}
