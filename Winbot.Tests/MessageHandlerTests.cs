﻿/***************************************************************************
 * 
 *   copyright: Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace Winbot.Tests
{
    [TestFixture]
    public class MessageHandlerTests
    {
        private Robot myrobot;

        [SetUp]
        public void setup()
        {
            myrobot = new Robot("My robot", @"\Scripts");
        }

        [Test]
        public void can_create_new_message_handler()
        {
            DelegateHandler handler = new DelegateHandler("hello", (robot,msg) => robot.Send("Hello world"));

            Assert.AreEqual("hello", handler.Pattern);
        }

        [Test]
        public void can_call_message_handler_callback_if_message_is_satisfied()
        {
            bool isSatisfied = false;
            bool isSatisfied2 = false;

            DelegateHandler handler = new DelegateHandler("hello", (robot, msg) => isSatisfied = true);
            DelegateHandler handler2 = new DelegateHandler("another", (robot, msg) => isSatisfied = true);

            handler.Call(myrobot, "hello");
            handler2.Call(myrobot, "hello");

            Assert.AreEqual(true, isSatisfied);
            Assert.AreEqual(false, isSatisfied2);
        }
    }
}
