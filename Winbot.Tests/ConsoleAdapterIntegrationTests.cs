﻿/***************************************************************************
 * 
 *   copyright: Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;

namespace Winbot.Tests
{
    [TestFixture]
    public class ConsoleAdapterIntegrationTests
    {
        private Robot myrobot;

        [SetUp]
        public void setup()
        {
            myrobot = new Robot();
        }

        private string buildOutputString(string message)
        {
            return string.Format("#Winbot> {0}{1}", message, Environment.NewLine);
        }

        [Test]
        public void consoleadapter_can_send_message()
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);

                ConsoleAdapter adapter = new ConsoleAdapter(myrobot);
                adapter.Send("Hello world!");

                Assert.AreEqual(buildOutputString("Hello world!"), writer.ToString());
            }
        }

        [Test]
        public void consoleadapter_can_raise_an_event_on_message_received()
        {
            string msg = string.Empty;

            ConsoleAdapter adapter = new ConsoleAdapter(myrobot);
            adapter.OnMessageReceived += (sender, e) =>
            {
                msg = e.Message;
            };

            adapter.Receive("test");

            Assert.AreNotEqual(string.Empty, msg);
            Assert.AreEqual("test", msg);
        }

        [Test]
        public void consoleadapter_can_respond_if_empty_input()
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);

                ConsoleAdapter adapter = new ConsoleAdapter(myrobot);

                adapter.Receive("");

                Assert.AreEqual(buildOutputString("Sorry can't understand that"), writer.ToString());
            }
        }

        [Test]
        public void robot_can_respond_to_message()
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);

                myrobot.Respond("hi|hello", (robot,msg) => robot.Send(string.Format("Hey #{0}, how u doing?", Environment.UserName)));
                myrobot.Receive("hello Winbot");

                Assert.AreEqual(buildOutputString("Hey #quasenada, how u doing?"), writer.ToString());
            }
        }

        [Test]
        public void robot_can_execute_an_action_based_on_message_received()
        {
            myrobot.Hear("hello|hi", (robot,msg) => robot.Send("Hello world"));

            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);

                myrobot.Receive("hello");

                Assert.AreEqual(buildOutputString("Hello world"), writer.ToString());
            }
        }

        [Test]
        public void can_check_on_message_received_inside_handler()
        {
            using (StringWriter writer = new StringWriter())
            {
                Console.SetOut(writer);

                DelegateHandler handler = new DelegateHandler("say this word: ([a-z]*)?", (robot, msg) =>
                {
                    int delPos = msg.Message.IndexOf(":") + 1;
                    string word = msg.Message.Substring(delPos, msg.Message.Length - delPos);

                    robot.Send("You said: " + word.Trim());
                });
                myrobot.Hear(handler);
                myrobot.Receive("say this word: silly");

                Assert.AreEqual(buildOutputString("You said: silly"), writer.ToString());
            }
        }
    }
}
