﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using Winbot;
using System.IO;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Net;
using System.Collections;
using System.Text.RegularExpressions;

namespace WinbotConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Robot myrobot = new Robot();
            myrobot.Run();

            Console.WriteLine("Press any key to continue ...");
            Console.Read();
        }
    }
}
