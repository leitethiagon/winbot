﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Winbot
{
    public abstract class Handler
    {
        public virtual string Help()
        {
            return "";
        }

        public abstract string Pattern { get; }
        public abstract void Callback(Robot robot, TextMessage msg);

        private Regex _regex;

        /// <summary>
        /// Call a handler call backup if message satisfies the regex pattern
        /// </summary>
        /// <param name="message"></param>
        public void Call(Robot robot, string message)
        {
            if(_regex == null)
                _regex = new Regex(Pattern);

            if (_regex.IsMatch(message))
                Callback(robot, new TextMessage(_regex, message));
        }
    }
}
