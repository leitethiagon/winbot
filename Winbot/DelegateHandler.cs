﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Winbot
{
    public class DelegateHandler : Handler
    {
        public DelegateHandler(string pattern, Action<Robot, TextMessage> callback)
        {
            _pattern = pattern;
            _callback = callback;
        }

        private string _pattern;
        private Action<Robot, TextMessage> _callback;

        public override string Pattern 
        {
            get { return _pattern; } 
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            _callback(robot, msg);
        }

        public void Callback(Robot robot)
        {
            _callback(robot, null);
        }

        public override string Help()
        {
            return string.Empty;
        }
    }
}
