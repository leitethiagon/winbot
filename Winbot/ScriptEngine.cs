﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.CodeDom.Compiler;

namespace Winbot
{
    public class ScriptEngine
    {
        public ScriptEngine(string scriptsDirectory, string fileExtension)
        { 
            ScriptsDirectory = scriptsDirectory;
            FileExtension = fileExtension;

            _errors = new StringBuilder();
        }

        public string FileExtension { get; private set; }
        public string ScriptsDirectory { get; private set; }
        public Assembly CompiledAssembly { get; set; }

        public string Errors 
        { 
            get { return _errors.ToString(); } 
        }

        public bool Compile()
        {
            _errors = new StringBuilder();

            string[] files = GetScripts(ScriptsDirectory, FileExtension);

            if (files.Length == 0)
            {
                _errors.AppendLine("No scripts found");

                return false;
            }

            try
            {
                CodeCompiler compiler = new CodeCompiler();
                CompiledAssembly = compiler.CompileCodeOrGetFromCache(files, "_cache");

                return true;
            }
            catch (CompilerErrorException ex)
            {
                _errors.AppendLine("Compiler errors:");
                foreach (CompilerError error in ex.Errors)
                {
                    _errors.AppendLine(string.Format("Line {0},{1}\t: {2}\n", error.Line, error.Column, error.ErrorText));
                }
            }
            catch (Exception ex)
            {
                _errors.AppendLine(ex.Message);
            }

            return false;
        }

        private StringBuilder _errors;

        private string[] GetScripts(string directory, string filter)
        {
            List<string> list = new List<string>();

            GetScripts(list, Path.Combine(Environment.CurrentDirectory, ScriptsDirectory), filter);

            return list.ToArray();
        }

        private void GetScripts(List<string> list, string path, string filter)
        {
            foreach (string dir in Directory.GetDirectories(path))
                GetScripts(list, dir, filter);

            list.AddRange(Directory.GetFiles(path, filter));
        }
    }
}
