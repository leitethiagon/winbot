﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Winbot
{
    public class TextMessage
    {
        public string Message { get; private set; }
        private Regex _regex;

        public TextMessage(Regex regex, string message)
        {
            Message = message;
            _regex = regex;
        }

        public Match Match()
        {
            return _regex.Match(Message);
        }
    }
}
