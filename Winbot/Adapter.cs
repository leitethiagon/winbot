﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Winbot
{
    public abstract class Adapter
    {
        protected Robot Robot { get; set; }
        public abstract void Run();
        public abstract void Send(string message);
        public abstract void Receive(string message);

        public event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        public void RaiseMessageReceivedEvent(string message)
        {
            if (OnMessageReceived != null)
                OnMessageReceived(this, new MessageReceivedEventArgs() { Message = message });
        }
    }
}
