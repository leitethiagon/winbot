﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.CodeDom.Compiler;
using Winbot.Scripts;

namespace Winbot
{
    public class Robot
    {
        public Robot()
        {
            Name = "Winbot";
            ScripsPath = @"Scripts";
            Handlers = new List<Handler>();
            _isRunning = false;
            _startTime = 0;
        }

        public Robot(string name, string scriptsPath)
        {
            Name = name;
            ScripsPath = scriptsPath;
            Handlers = new List<Handler>();
            _isRunning = false;
            _startTime = 0;
        }

        private long _startTime;

        public TimeSpan Uptime
        {
            get
            {
                long _elapsedTime = DateTime.Now.Ticks;

                return new TimeSpan(_elapsedTime - _startTime);
            }
        }

        public string Name { get; private set; }
        public string ScripsPath { get; set; }

        public List<Handler> Handlers { get; private set; }

        public Adapter DefaultAdapter
        {
            get { return new ConsoleAdapter(this); }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
        }

        /// <summary>
        /// Add a new message handler
        /// </summary>
        /// <param name="pattern">string with regex pattern</param>
        /// <param name="callback">callback function to call</param>
        public void Hear(string pattern, Action<Robot,TextMessage> callback)
        {
            Handlers.Add(new DelegateHandler(pattern, callback));
        }

        /// <summary>
        /// Add new message handler
        /// </summary>
        /// <param name="handler">MessageHandler instance</param>
        public void Hear(Handler handler)
        {
            Handlers.Add(handler);
        }

        /// <summary>
        /// Add a new message handler for when the name of the robot is mentioned
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="callback"></param>
        public void Respond(string pattern, Action<Robot,TextMessage> callback)
        {
            pattern = string.Format("{0} {1}", Name, pattern);

            Handlers.Add(new DelegateHandler(pattern, callback));
        }

        /// <summary>
        /// Receive a message and check if it matches any handler
        /// </summary>
        /// <param name="message">String message</param>
        public void Receive(string message)
        {
            foreach (var handler in Handlers)
            {
                handler.Call(this, message);
            }
        }

        /// <summary>
        /// Run the robot
        /// </summary>
        public void Run()
        {
            _startTime = DateTime.Now.Ticks;

            Load();

            _isRunning = true;
            DefaultAdapter.Run();
        }

        public void DisplayHelp()
        {
            foreach (var handler in Handlers)
            {
                if (string.IsNullOrEmpty(handler.Help()) == false)
                    Send(handler.Help());
            }
        }

        /// <summary>
        /// send a message
        /// </summary>
        /// <param name="message"></param>
        public void Send(string message)
        {
            DefaultAdapter.Send(message);
        }

        private bool _isRunning;

        /// <summary>
        /// Load and setup scripts
        /// </summary>
        public void Load()
        {
            Handlers = new List<Handler>();
            AddDefaultHandlers();

            Send("Loading scripts ...");

            ScriptEngine scriptsEngine = new ScriptEngine(ScripsPath, "*.cs");
            if (scriptsEngine.Compile())
            {
                foreach (Type type in scriptsEngine.CompiledAssembly.GetTypes())
                {
                    var handler = (Handler)Activator.CreateInstance(type);
                    Hear(handler);
                }

                Send("Scripts loaded");
            }
            else Send(scriptsEngine.Errors);
        }

        private void AddDefaultHandlers()
        {
            Handlers.Add(new HelpHandler());
            Handlers.Add(new ShutdownHandler());
            Handlers.Add(new UptimeHandler());
            Handlers.Add(new LoadScriptsHandler());
            Handlers.Add(new GreetingsHandler());
        }

        public void Shutdown()
        {
           _isRunning = false;
        }
    }
}
