﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Winbot;

namespace Winbot.Scripts
{
    public class GreetingsHandler : Handler
    {
        public override string Pattern
        {
            get { return "(?:hello|hi) winbot"; }
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            robot.Send("Hello there");
        }
    }
}
