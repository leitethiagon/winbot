﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Winbot;

namespace Winbot.Scripts
{
    public class HelpHandler : Handler
    {
        public override string Help()
        {
            return "help - display all available handlers";
        }

        public override string Pattern
        {
            get { return @"\b(help)\b"; }
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            robot.DisplayHelp();
        }
    }
}
