﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Winbot.Scripts
{
    public class UptimeHandler : Handler
    {
        public override string Help()
        {
            return "uptime - return uptime of this robot instance";
        }

        public override string Pattern
        {
            get { return @"\b(uptime)\b"; }
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            TimeSpan uptime = robot.Uptime;
            robot.Send(string.Format("{0} days, {1} hours, {2} minutes, {3} seconds", 
                uptime.Days, uptime.Hours, uptime.Minutes, uptime.Seconds));
        }
    }
}
