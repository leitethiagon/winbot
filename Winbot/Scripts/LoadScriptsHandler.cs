﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Winbot.Scripts
{
    public class LoadScriptsHandler : Handler
    {
        public override string Help()
        {
            return "reload - reload scripts";
        }

        public override string Pattern
        {
            get { return @"\b(reload)\b"; }
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            robot.Load();
        }
    }
}
