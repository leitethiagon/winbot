﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Winbot.Scripts
{
    public class ShutdownHandler : Handler
    {
        public override string Help()
        {
            return "shutdown - turn off the robot";
        }

        public override string Pattern
        {
            get { return @"\b(shutdown)\b"; }
        }

        public override void Callback(Robot robot, TextMessage msg)
        {
            robot.Shutdown();
        }
    }
}
