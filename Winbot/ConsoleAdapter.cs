﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Winbot
{
    public class ConsoleAdapter : Adapter
    {
        public ConsoleAdapter(Robot robot)
        {
            Robot = robot;
        }

        public override void Send(string message)
        {
            Console.WriteLine(string.Format("{0}{1}", RobotPrefix, message));
        }

        public override void Receive(string message)
        {
            if (string.IsNullOrEmpty(message))
                Send("Sorry can't understand that");

            Robot.Receive(message);

            RaiseMessageReceivedEvent(message);
        }

        public override void Run()
        {
            Send("Running ...");
            string input = "";

            do
            {
                Console.Write(CurrentUserPrefix);
                input = Console.ReadLine();
                Robot.Receive(input);
            }
            while (Robot.IsRunning);
        }

        private string RobotPrefix
        { 
            get { return string.Format("#{0}> ", Robot.Name); } 
        }

        private string CurrentUserPrefix
        {
            get { return string.Format("#{0}> ", CurrentUser); } 
        }

        private string CurrentUser
        {
            get { return Environment.UserName; }
        }
    }
}
