﻿/***************************************************************************
 * 
 *   copyright(c) 2012 Thiago Leite
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version
 *   
 ***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;

namespace Winbot
{
    public class CompilerErrorException : Exception
    {
        public CompilerErrorCollection Errors { get; private set; }

        public CompilerErrorException(CompilerErrorCollection errors)
        {
            Errors = errors;
        }
    }
}
